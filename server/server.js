let express = require("express");
const server = express();
const IP = "127.0.0.1";
const PORT = 8081;

const http = require("http");
const { Socket } = require("net");
const webServer = http.Server(server);

const socketIo = require("socket.io");
let io = socketIo(webServer, {transports: ["websocket"]});  //creating web socket server

io.on("connection", (socket)=> {
    console.log("User Online");

    socket.on("canvas-data", (data)=> {     //boadcasting all other connected users
        io.emit("canvas-data", data);
    });
    socket.on("disconnect", () => console.log("Client lost connection."));
});
webServer.listen(PORT,IP, () => console.log(`http://${IP}:${PORT}/`))
