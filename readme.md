# Collaborative white board 
In this project I have created a collaborative white board using Node JS, React and Web Socket protocol
## Advantages :-
* Collaborative white board is a useful tool for remote work.
* Multiple users can share an online board, where user can write and draw different text or diagram, and all other users can see it in real time.
* If we want to describe some thing to our colleagues or trying to make our student understand something using a diagram, Collaborative white board is the best way to do so.
* It works with much less band width than a screen sharing software.
* Also allows multiple users to interact at a time.
## Project description
Inside this project folder I have created two sub folders “server” and “white_board”.<br>
"white_board" folder for the React app and server folder for the Node js.<br>
In the React folder I have created two Components.
* Container component 
* Board component <br>
<br>
## white_board folder :
Container components containing the Container.js file and for styling styling.css file. 
Similarly in the Board component containing the Board.js file and for styling styling .css file.

## Container component
Container.js file first import React and  have import border file 
And also created constructor and render function
By default, the drawing color is black. For change the color, changeColor () function used. The same in the case of pencil size changeSize() function is used
So with this app multiple users can draw at a time.
## Board component
In board.js file first import the React, socket.io-client and the css file
I have installed socket io client :- The protocol defines the format of the packets exchanged between the client and the server.

### To receive the server emitted event :-
When canvas data event will be received a new image will be drawn in the canvas,
then the image will be constructed using the data which is received from server. The  data is basically the base 64 encoded image string which was emitted from another user’s drawing, that drawing will now be displayed in all other users canvas.
### componentDidMount() :-
For invoking the “drawOnCanvas” function I have defined a lifecycle hook method of react “componentDidMount”. It is equivalent to window.onload in conventional java script. Inside this function just call this.drawOnCanvas function.
### componentWillReceiveProps() :-
To receive updated properties I used a hook method for react components that is componentWillReceiveProps. Which will take new properties as argument. Inside this method  set the canvas context stroke style and line width as per the properties color and pencil size.
<br>
<br>
I have used a setTime out function. Normally we don’t want to use setTimeOut function but here I used, because I don’t want to send image data to all other users for each point draw rather I want the user to complete a drawing segment.
This will help to reduce bandwidth usage significantly. So setTimeOut help to send image data only when user will pass for a second.

## server folder :
In server folder first I installed the libraries express and socket-io and also server.js file for node.js code.
So, inside server.js file first import all required libraries and after that I have created a http server this server will run the port 8081.
After that I have created a web socket server. So, whenever a user will be connected with a websocket server user online message will be shown in the console.
And finally if server receives any event with name canvas data the message will be broadcasted to all other connected users.<br>
## Sample collaborative white boards:-
![alt text](Collaborative_white_board_screenshot.jpg)


