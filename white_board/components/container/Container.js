import React from "react";              //import react
import Board from "../board/Board";     //import board component

import "./style.css";

//create class component which will extend react.component
class Container extends React.Component
{
    constructor(props) {
        super(props);

        //defining the state object
        this.state = {
            color: "#000000",
            size: "5"
        }

    }

    changeColor(params) {
        this.setState({
            color: params.target.value
        })
    }

    changeSize(params) {
        this.setState({
            size: params.target.value
        })
    }

    render() {

        return (
            <div className="container">
                <div className="tools-section">
                <div className="color-picker-container">
                    Select Pencil color : &nbsp;
                    <input type="color" value={this.state.color} onChange={this.changeColor.bind(this)}/>
                    <button type="button" id="clearbutton" className="button">Clear</button>
                    <button type="button" id="textbutton" className="button">Text</button>
                </div>

                <div className="brushsize-container">
                        Select Pencil Size : &nbsp; 
                        <select value={this.state.size} onChange={this.changeSize.bind(this)}>
                            <option> 5 </option>
                            <option> 10 </option>
                            <option> 15 </option>
                            <option> 20 </option>
                            <option> 25 </option>
                            <option> 30 </option>
                        </select>
                    </div>
                    </div>

                <div className="board-container">
                    {/* passing color and size to board component */}
                <Board color={this.state.color} size={this.state.size}></Board>
                </div>

            </div>
        )
    }
}

export default Container;       //export the class