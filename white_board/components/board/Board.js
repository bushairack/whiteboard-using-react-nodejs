import React from "react";
import io from "socket.io-client";

import "./style.css";

//create class component which will extend react.component
class Board extends React.Component {

    timeout;
    socket = io.connect("http://localhost:8081", {transports: ["websocket"]});  //server running port

    ctx;        //class level variable
    isDrawing = false;

    constructor(props) {
        super(props);

        this.socket.on("canvas-data", function(data){   //To receive the server emitted event 

            let root = this;
            let interval = setInterval(function(){
            if(root.isDrawing) return;
            root.isDrawing = true;
            clearInterval(interval);

            let image = new Image();
            let canvas = document.querySelector("#board");
            let ctx = canvas.getContext("2d");
            image.onload = function() {
                ctx.drawImage(image, 0, 0);

                root.isDrawing = false;
            };
            image.src = data;
        }, 200)
        })
    }

    componentDidMount() {           //lifecycle hook method of react
        this.drawOnCanvas();
    }

    //To recieve updated properties 
    componentWillReceiveProps(newProps) {
        this.ctx.strokeStyle = newProps.color;
        this.ctx.lineWidth = newProps.size;
    }

    drawOnCanvas() {
        let canvas = document.querySelector('#board');
        this.ctx = canvas.getContext('2d');
        let ctx = this.ctx;

        let sketch = document.querySelector('#sketch');
        let sketch_style = getComputedStyle(sketch);
        canvas.width = parseInt(sketch_style.getPropertyValue('width'));
        canvas.height = parseInt(sketch_style.getPropertyValue('height'));

        let mouse = { x: 0, y: 0 };
        let last_mouse = { x: 0, y: 0 };

        /* Mouse Capturing Work */
        canvas.addEventListener('mousemove', function (e) {
            last_mouse.x = mouse.x;
            last_mouse.y = mouse.y;

            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
        }, false);


        /* Drawing on Paint App */
        ctx.lineWidth = this.props.size;
        ctx.lineJoin = 'round';
        ctx.lineCap = 'round';
        ctx.strokeStyle = this.props.color;

        canvas.addEventListener('mousedown', function (e) {
            canvas.addEventListener('mousemove', onPaint, false);
        }, false);

        canvas.addEventListener('mouseup', function () {
            canvas.removeEventListener('mousemove', onPaint, false);
        }, false);

        let root = this;        //this key word point to the inner function not the Board class
        let onPaint = function () {
            ctx.beginPath();
            ctx.moveTo(last_mouse.x, last_mouse.y);
            ctx.lineTo(mouse.x, mouse.y);
            ctx.closePath();
            ctx.stroke();

            sendData();
        };
        let clearData = document.querySelector("#clearbutton");
        clearData.addEventListener("click", function(){
            ctx.fillStyle = "white";
            ctx.clearRect(0,0,canvas.width, canvas.height);
            ctx.fillRect(0,0,canvas.width, canvas.height);

            sendData();
        });


        // For texting

        let textData = document.querySelector("#textbutton");
        textData.addEventListener("click", function() {
            let context = canvas.getContext("2d");
            
            // Variables to store mouse x and mouse y positions
            let mouseX = 0;
            let mouseY = 0;
            let startingX = 0;

            // An array to store every word
            let recentWords = [];

            // An array for backspace
            let undoList = [];

            // Function for save canvas 
            function saveState() {
                undoList.push(canvas.toDataURL());
            }

            saveState();

            // function for backspace
            function undo (){
                undoList.pop();

                let imgData = undoList[undoList.length - 1];
                let image = new Image();

                //Display old saved state
                image.src = imgData;
                image.onload = function () {
                    context.clearRect(0, 0, canvas.width, canvas.height);
                    context.drawImage(image, 0, 0, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
                };
            }

            // function for mouse click event
            canvas.addEventListener("click", function(e) {
                mouseX = e.pageX - canvas.offsetLeft;
                mouseY = e.pageY - canvas.offsetTop;
                startingX = mouseX;

                // Restart recent words array
                recentWords = [];

                return false
            });

            // key down event
            document.addEventListener("keydown", function(e) {
                context.fillStyle = "black";
                context.font = "26px Arial";

                if(e.keyCode === 8) {
                    //  Backspace is pressed
                    undo();

                    let recentWord = recentWords[recentWords.length - 1];

                    // Move cursor back
                    mouseX -= context.measureText(recentWord).width;
                    recentWords.pop();
                } else if (e.keyCode === 13) {
                    //Enter key is pressed
                    mouseX = startingX;
                    mouseY +=20;    // The size of font + 4
                }else{
                    //write text to canvas
                    
                    context.fillText(e.key, mouseX, mouseY);

                    //Move cursor forward after every key press
                    mouseX += context.measureText(e.key).width;

                    saveState();
                    recentWords.push(e.key);
                }
                sendData();
            });
        });




        function sendData(){
            if(root.timeout != undefined) clearTimeout(root.timeout); //ensure to execute a single time out function
            root.timeout = setTimeout(function(){
                let imageData = canvas.toDataURL("image/png");  //base 64 encoded image data from canvas
                root.socket.emit("canvas-data", imageData); //emit the base 64 encoded image string to server
            }, 1000)
        }
    }
    
    
    render() {
        return (
            <div className="sketch" id="sketch">
                <canvas className="board" id="board"></canvas> 
            </div>
            
        )
    }
}

export default Board;       //export the class

